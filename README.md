# (WORK IN PROGRESS PROJECT)

# Table of Contents

1.  [Project overview](#org38864d3)
    1.  [Tech stack](#org61fbf30)
    2.  [Requirements](#org3609a6c)
    3.  [Details](#org2c4a548)
2.  [Installation](#orgc1945a0)
    1.  [Localstack](#orgf6899ce)
    2.  [NodeJS dependencies / Serverless framework plugins](#orgf6899cf)
    3.  [DynamoDB](#orga32c45c)
        1.  [Get list of tables](#orga9d5bee)
        2.  [Insert example data to table](#orgdb412f7)
        3.  [List content of table](#org6a8c416)
        4.  [(TBD) Show example data from API Gateway REST API](#org103f5b8)
        5.  [(TBD) Add data to table using Lambda function](#org3907da3)
    4.  [Destroy infrastructure / resources](#orge3979b4)
    5.  [Troubleshooting](#orge3979b5)


<a id="org38864d3"></a>

# Project overview


<a id="org61fbf30"></a>

## Tech stack


<a id="orgbf98c99"></a>

#### Localstack - AWS local development framework


<a id="org72818f4"></a>

#### Serverless Framework (AWS)


<a id="orgf4ac2aa"></a>

#### DynamoDB


<a id="orgd7d84df"></a>

#### Artifact Registry (DockerHub registry)


<a id="orge8b8c02"></a>

#### PostgreSQL (ingress connection with NodeJS app in k8s pod)


<a id="org0641130"></a>

#### NodeJS app (with example tests)


<a id="org3609a6c"></a>

## Requirements


<a id="orga730feb"></a>

- localstack (ver. 1.3.1)

GitHub link for project: https://github.com/localstack/localstack


<a id="org5683384"></a>

- Docker (ver. 20.10.5)


<a id="org32d7b07"></a>

- awslocal (AWS CLI for localstack ver. 1.27.53)

GitHub link for project: https://github.com/localstack/awscli-local

<a id="orgac5a59e"></a>

- Serverless CLI

GitHub link for project: https://www.serverless.com/console/docs

Serverless CLI versions:
```
Framework Core: 3.27.0
Plugin: 6.2.3
SDK: 4.3.2
```

- NodeJS (ver. 14.20.1)

- NPM (ver. 6.14.17)


<a id="org2c4a548"></a>

## Details


<a id="org86c7ead"></a>

- Only local development preview after installation locally is available (currently app is not provisioned on live server)


<a id="org30a1e58"></a>

- Locally deployed app should show example data inserted manually in DynamoDB when CLI command is run


<a id="orgc1945a0"></a>

# Installation

<a id="orgf6899cf"></a>

## NodeJS dependencies / Serverless framework plugins

Requirements: 
- NodeJS (ver. 14.20.1)
- NPM (ver. 6.14.17)

1. Install NodeJS dependencies (Serverless framework plugins)

```sh
npm install
```


<a id="orgf6899ce"></a>

## Localstack

1. Run localstack

```sh
localstack start
```

2. Deploy app to localstack

```sh
serverless deploy --stage local
```


<a id="orga32c45c"></a>

## DynamoDB


<a id="orga9d5bee"></a>

### Get list of tables

Check if table `customerTable` is created

```sh
awslocal dynamodb list-tables
```


<a id="orgdb412f7"></a>

### Insert example data to table

```sh
awslocal dynamodb put-item \
    --table-name customerTable \
    --item file://item.json \
    --return-consumed-capacity TOTAL \
    --return-item-collection-metrics SIZE
```


<a id="org6a8c416"></a>

### List content of table

Show all items in table `customerTable`
```sh
awslocal dynamodb scan \
  --table-name customerTable \
  --endpoint-url 'http://localhost:4566'
```

Show chosen records in table `customerTable`
```sh
awslocal dynamodb get-item \
    --table-name customerTable \
    --key '{"id":{"S":"1"},"email":{"S":"example@gmail.com"}}' \
    --consistent-read
```

<a id="org103f5b8"></a>

### (TBD-currently returns error) Show example data from API Gateway REST API

Obtain REST API ID

```sh
awslocal apigateway get-rest-apis | jq -r '.items[] | select(.name="local-localstack-demo") | .id'
```

Use REST API ID to get data from API Gateway REST API

```sh
curl http://localhost:4566/restapis/<REST_API_ID>/local/_user_request_/getCustomers
```

<a id="org3907da3"></a>

### (TBD-currently returns error) Add data to table using Lambda function

Currently after running program, the error occurs. The problem is with connection to DynamoDB table
The solution to problem should be fixing the test credentials for localstack environment inside Go program

Build Go binary
```sh
cd goapp
env GOOS=linux GOARCH=386 CGO_ENABLED=0 go build -gcflags="all=-N -l" -o bin/createCustomer/main main.go
```

Run Go binary
```sh
./bin/createCustomer/main
```

<a id="orge3979b4"></a>

## Destroy infrastructure / resources

```sh
localstack stop
```

<a id="orge3979b5"></a>

## Troubleshooting

Debug app deployment to localstack

```sh
serverless deploy --stage local --debug=true
```

Show logs from localstack

```sh
localstack logs
```

Show config for localstack

```sh
localstack config show
```

Check cloudformation status / details

```sh
awslocal cloudformation list-stack-resources --stack-name goapp
```

Check cloudformation events

```sh
awslocal cloudformation describe-stack-events --stack-name goapp
```

Validate generated cloudformation template

```sh
awslocal cloudformation validate-template --debug --template-body .serverless/cloudformation-template-update-stack.json
```
