package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"os"
)

func main() {
	fmt.Println("Hello World")

	_ = os.Setenv("_LAMBDA_SERVER_PORT", "9001")
	_ = os.Setenv("AWS_LAMBDA_RUNTIME_API", "127.0.0.1:9001")
	_ = os.Setenv("AWS_SECRET_ACCESS_KEY", "test")
	_ = os.Setenv("AWS_ACCESS_KEY_ID", "test")

	request := events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Path:       "/createCustomer",
		Body:       `{"id":"123","email": "example3@gmail.com"}`,
	}
	response, err := Handler(request)
	if err != nil {
		fmt.Println("Handler error")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	println(response.Body)
}

type MyRequest struct {
	Id    string `json:"id"`
	Email string `json:"email"`
}

func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	headers := make(map[string]string)
	if request.Headers != nil {
		headers = request.Headers
	}
	token := "AWS4-HMAC-SHA256 Credential=testkey/date/us-east-1/apigateway/aws4_request"
	headers["Authorization"] = token
	headers["Content-Type"] = "application/json"

	background := context.Background()
	background = context.WithValue(background, "aws_config_key", aws.Credentials{
		AccessKeyID:     "test",
		SecretAccessKey: "test",
	})

	cfg, err := config.LoadDefaultConfig(background,
		config.WithEndpointResolver(aws.EndpointResolverFunc(
			func(service, region string) (aws.Endpoint, error) {
				return aws.Endpoint{URL: "http://localhost:4566"}, nil
			})),
	)
	if err != nil {
		fmt.Println(err)

		return events.APIGatewayProxyResponse{
			Body:            "Error loading config",
			Headers:         headers,
			IsBase64Encoded: false,
			StatusCode:      500,
		}, err
	}
	fmt.Println("Before dynamodb.NewFromConfig")
	svc := dynamodb.NewFromConfig(cfg)

	var myRequest MyRequest
	err = json.Unmarshal([]byte(request.Body), &myRequest)
	if err != nil {

		fmt.Println("Error unmarshalling request body")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	myRequest.Id = "2"
	myRequest.Email = "example@gmail.com"

	input1 := &dynamodb.ListTablesInput{}
	result1, err := svc.ListTables(background, input1)
	if err != nil {
		fmt.Println("Got error calling ListTables:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	fmt.Println("Tables:")
	for _, n := range result1.TableNames {
		fmt.Println(n)
	}
	cfg.Region = "us-east-1"
	cfg.EndpointResolver = aws.EndpointResolverFunc(
		func(service, region string) (aws.Endpoint, error) {
			return aws.Endpoint{URL: "http://localhost:4566"}, nil
		},
	)

	inputDescribeTable := &dynamodb.DescribeTableInput{
		TableName: aws.String("customerTable"),
	}
	_, err = svc.DescribeTable(background, inputDescribeTable)
	if err != nil {
		fmt.Println("Got error calling DescribeTable:")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	input := &dynamodb.PutItemInput{
		TableName: aws.String("customerTable"),
		Item: map[string]types.AttributeValue{
			"id": &types.AttributeValueMemberS{
				Value: myRequest.Id,
			},
			"email": &types.AttributeValueMemberS{
				Value: myRequest.Email,
			},
		},
	}

	_, err = svc.PutItem(background, input)

	if err != nil {
		fmt.Println("Got error calling PutItem:")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return events.APIGatewayProxyResponse{
		Body:            "Successfully added item to table",
		Headers:         headers,
		IsBase64Encoded: false,
		StatusCode:      200,
	}, err
}
